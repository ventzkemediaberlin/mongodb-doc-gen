require('dotenv-json')();
const parseSchema = require('mongodb-schema');
const MongoClient = require('mongodb').MongoClient;
const fs = require('fs');

const init = async () => {
  // Initial vars
  const connectionString = process.env.MONGO_URI;
  const dbNames = process.env.DB_NAMES.split(',');
  const tableNames = process.env.TABLE_NAMES.split(',');
  const descriptions = process.env.DESCRIPTIONS;

  // Build up schema and HTML
  const schemas = await collectSchemas(connectionString, dbNames, tableNames);
  const flattenedSchemas = flattenSchemas(schemas);
  const htmlDoc = generateHtmlDoc(flattenedSchemas, dbNames, descriptions);

  // Save it
  fs.writeFile('./docs/'+dbNames.join('__')+'.html', htmlDoc, function(err) {
    console.log('Docs generated');
  });
  fs.writeFile('./docs/'+dbNames.join('__')+'.json', JSON.stringify(flattenedSchemas, null, 2), function(err) {
    console.log('Schema generated');
  }); 
}

const collectSchemas = async (connectionString, dbNames, tableNames) => {
  const schemas = {};

  const client = await MongoClient.connect(connectionString, { useNewUrlParser: true });

  for(const dbName of dbNames) {
    const db = client.db(dbName);
    const opts = {
      storeValues: false, 
      semanticTypes: true
    }
    
    for (const tableName of tableNames) {
      // Wait for schema to generate
      const schema = await new Promise((resolve, reject) => {
        parseSchema(db.collection(tableName).find(), opts, function(error, schema) {
          if (error) reject(error);
          resolve(schema);
        });
      });

      // Add schema if exists
      if (schema.fields && schema.fields.length > 0) {
        schema.dbName = dbName;
        schemas[tableName] = schema;
      }
    }
  }

  client.close();

  return schemas;
}

const generateHtmlDoc = (schemas, dbNames, descriptions) => {
  const html = `
    <html>
      <head>
        <style>
          body {
            font-family: Helvetica;
            font-color: #333;
          }

          table {
            width: 100%;
            margin-bottom: 40px;
          }

          th {
            text-align: left;
            padding: 3px;
            background: #657EFF;
            color: white;
          }

          td {
            width: 33%;
            padding: 3px;
          }

          tr:nth-child(2n) td {
            background: #D7DEFF;
          }
        </style>
      </head>
      <body>
        <h1>DB Schema for ${dbNames.join(' and ')}</h1>

        ${Object.keys(schemas).map((key, index) => {
          return generateHtmlTable(schemas[key], key, descriptions);
        }).join('')}
      </body>
    </html>
  `;

  return html;
}

const generateHtmlTable = (schema, tableName, descriptions) => {
  const table = `
    <h2>Model Description</h2>
    <h3>DB name: ${schema.dbName}</h3>
    <h3>Table name: ${tableName}</h3>
    <table>
      <thead>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
      </thead>
      <tbody>
        ${schema.fields.map((field) => {
          if (Array.isArray(field.type)) {
            field.type = field.type.join(',');
          }

          let description = '';
          if (typeof descriptions[field.path] !== 'undefined') {
            description = descriptions[field.path];
          }
          if (typeof descriptions[tableName] !== 'undefined' && typeof descriptions[tableName][field.path] !== 'undefined') {
            description = descriptions[tableName][field.path];
          }

          return `<tr>
            <td>${field.path}</td>
            <td>${field.type}</td>
            <td>${description}</td>
          </tr>`;
        }).join('')}
      </tbody>
    </table>
  `;

  return table;
}

// Sometimes we have nested documents inside a schema, so we flatten them and add them onto the schema.fields array
const flattenSchemas = (schemas) => {
  let flattenedSchemas = {...schemas};

  Object.keys(schemas).map(function(schemaKey, index) {
    const schema = schemas[schemaKey];

    // They are nested nested nested... so have to recursively go through and get fields from fields...
    const flattenFields = (fields) => {
      let flattenedFields = [...fields];

      for (const field of fields) {
        for (const type of field.types) {
          if (type.fields) {
            flattenedFields = flattenedFields.concat(flattenFields(type.fields));
          }
        }
      }

      return flattenedFields;
    }

    // Flatten
    const flattenedFields = flattenFields(schema.fields);
    flattenedSchemas[schemaKey].fields = flattenedSchemas[schemaKey].fields.concat(flattenedFields);

    // Remove duplicates
    flattenedSchemas[schemaKey].fields = flattenedSchemas[schemaKey].fields.reduce((acc, current) => {
      const x = acc.find(item => item.path === current.path);
      if (!x) {
        return acc.concat([current]);
      } else {
        return acc;
      }
    }, []);

    // Sort
    flattenedSchemas[schemaKey].fields = flattenedSchemas[schemaKey].fields.sort((a, b) => {
      if(a.path < b.path) { return -1; }
      if(a.path > b.path) { return 1; }
      return 0;
    });
  });

  return flattenedSchemas;
}

init();