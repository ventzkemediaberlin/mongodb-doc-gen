# mongodb-doc-gen
mongodb-doc-gen is a node script that can be used to generate a HTML document detailing a mongodb schema.

## Requirements
First install the required packages.
```bash
yarn install
```
or
```bash
npm install
```

## Usage
Edit the .env.json.example file to match your environment/database, and rename it to .env.json, then run the script.
```bash
yarn start
```
